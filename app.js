#!/usr/bin/env node

//This is the mindwave node application adapted from the 
//sharejs example. Some code here is probably irrelevant.

//Setup node and sharejs dependencies
require('coffee-script/register');
var express = require('express'),
  sharejs = require('./node_modules/share/src'),
  fs = require('fs');
  hat = require('hat').rack(32, 36);

//This should allow you to specify a port number when running
//the node server. For some reason, it won't run on port 80, which 
//is unfortunate. Defaults to port 8000 if nothing is specified.
var argv = require('optimist').
  usage("Usage: $0 [-p portnum]").
  default('p', 3000).
  alias('p', 'port').
  argv;

//This initializes express, a library that makes setting up node 
//applications easier.
var server = express();
server.use(express.static(__dirname + '/static'));

//These are options that you can set to customize how the sharejs app
//communicates between client and server.
//If we want, we could set it to use a database here to store information
//from the pad.
var options = {
  db: {type: 'none'},
  browserChannel: {cors: '*'},
  auth: function(client, action) {
    // This auth handler rejects any ops bound for docs starting with 'readonly'.
    if (action.name === 'submit op' && action.docName.match(/^readonly/)) {
      action.reject();
    } else {
      action.accept();
    }
  }
};

//This defines a function used below to return a random 
//string of a specified length.
function randomString(len) {
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  var string_length = len;
  var randomstring = '';
  for (var i=0; i<string_length; i++) {
    var rnum = Math.floor(Math.random() * chars.length);
    randomstring += chars.substring(rnum,rnum+1);
  }
  return randomstring;
}

//Global variables used for substitution of case-specific character.
var uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var lowers = "abcdefghijklmnopqrstuvwxyz";

//This takes a string s, splits by character and returns its reverse.
function reverseString(s){
    return s.split("").reverse().join("");
}

//This reads a file (taken from project gutenberg) and splits it into
//a list of paragraphs.
var paras;
fs.readFile('/home/node/mindwave-node/books/38503-0.txt', 'utf8', function (err, data) {
  if (err) {
    console.log(err);
  }
  paras = data.split('\n');
});

//This function returns a random paragraph from the text read above.
function randParagraph() {
  return paras[Math.floor(Math.random() * paras.length)];
}

// Lets try and enable redis persistance if redis is installed...
// try {
//   require('redis');
//   options.db = {type: 'redis'};
// } catch (e) {}

console.log("Mindwave Node application, using ShareJS v" + sharejs.version);
console.log("Options: ", options);

var port = argv.p;

// Attach the sharejs REST and Socket.io interfaces to the server
sharejs.server.attach(server, options);

var sjs = require('share').client;
var connection = new sjs.Connection('http://localhost:3000/channel');
connection.open('mindwave', 'text', function(error, doc) {
  if (error) {
    console.log(error);
  }
  
  //This is really kludgy
  //Point is to get rid of making multiple connections
  //every time information is received from the headset
  //by nesting this endpoint within a permanent connection
  //to the mindwave pad.

  //This defines an endpoint where we point the helper mindwave application averaging the values.
  //The callback function gives us access to the request which contains a query
  //object with 'a' (attentions) and 'm' (meditation) values. We should get blink
  //values as well at some point.
  server.get('/data', function(req, res, next) {


    //The leading + turns the value into an integer
    var att = +req.query['a'];
    var med = +req.query['m'];

    //Returns the current text of the mindwave pad
    txt = doc.getText();
    len = txt.length;

    //From here down we try to do things with the text of the pad based on our headset input.
    if (att < 25) {

      //Choosing a random letter to replace another random letter 
      //if attention is below 25.
      query = randomString(1);
      rep = randomString(1);
      res.write("query: " + query + '\nrep: ' + rep + "\n");

      for (var i = txt.length - 1; i >= 0; i--) {
        if (txt[i].toLowerCase() == query.toLowerCase()) {
          if (uppers.indexOf(txt[i]) > -1) {
            r = rep.toUpperCase();
            q = query.toUpperCase();
          }
          else {
            q = query.toLowerCase();
            r = rep.toLowerCase();
          }
          doc.submitOp([{d: q, p: i}, {i: r, p: i}]);
        }
      }
    }
    //If meditation is greater than 70, reverse all of the text.
    if (med > 75) {
      txt = doc.getText();
      len = txt.length;
      doc.del(0, len);
      doc.insert(0, reverseString(txt));
      // res.write(doc.getText());
    }
    //If meditation is lower than 25, append a random paragraph of text
    //to the end of the pad.
    else if (med < 25) {
      var para = randParagraph().trim();
      doc.insert(doc.getText().length, "\n" + para);

    }
    
    res.write(doc.getText());
    
    res.end();
  });
});

//This defines what happens when someone visits the root directory
//i.e. mindwave.s-1lab.org:8000
//It attaches a specific document to that address called mindwave
server.get('/?', function(req, res, next) {
  var docName;
  docName = "mindwave";
  res.writeHead(303, {location: '/' + docName});
  res.write('');
  res.end();
});

//Moved above...
//This defines an endpoint where we point the helper mindwave application averaging the values.
//The callback function gives us access to the request which contains a query
//object with 'a' (attentions) and 'm' (meditation) values. We should get blink
//values as well at some point.
// server.get('/data', function(req, res, next) {
//   // var docName;
//   // var connection = new shareClient.Connection("/");

//   //The leading + turns the value into an integer
//   var att = +req.query['a'];
//   var med = +req.query['m'];
//   // console.log(req.query['a']);
  
//   //This connects to ShareJS and returns the value of the pad named 'mindwave'
//   // var sjs = require('share').client;
//   // var content = sjs.open('mindwave', 'text', 'http://localhost:3000/channel',  function(error, doc) {
//     // console.log('oops');

//     //Returns the current text of the mindwave pad
//     txt = doc.getText();
//     len = txt.length;

//     //From here down we try to do things with the text of the pad based on our headset input.
//     if (att < 25) {

//       //Choosing a random letter to replace another random letter 
//       //if attention is below 25.
//       query = randomString(1);
//       rep = randomString(1);
//       res.write("query: " + query + '\nrep: ' + rep + "\n");

//       for (var i = txt.length - 1; i >= 0; i--) {
//         if (txt[i].toLowerCase() == query.toLowerCase()) {
//           if (uppers.indexOf(txt[i]) > -1) {
//             r = rep.toUpperCase();
//             q = query.toUpperCase();
//           }
//           else {
//             q = query.toLowerCase();
//             r = rep.toLowerCase();
//           }
//           doc.submitOp([{d: q, p: i}, {i: r, p: i}]);
//         }
//       }
//       // for (var i = indices.length - 1; i >= 0; i--) {
//       //   doc.insert(indices[i], rep);
//       // }
//       // doc.del(len - att, len);
//       // doc.insert(len, '\n' + randomString(att));
//       // res.write('deleted...\n');
//     }
//     //If meditation is greater than 70, reverse all of the text.
//     if (med > 75) {
//       txt = doc.getText();
//       len = txt.length;
//       doc.del(0, len);
//       doc.insert(0, reverseString(txt));
//       // res.write(doc.getText());
//     }
//     //If meditation is lower than 25, append a random paragraph of text
//     //to the end of the pad.
//     else if (med < 25) {
//       var para = randParagraph().trim();
//       doc.insert(doc.getText().length, "\n" + para);

//     }
//     res.write(doc.getText());

    
    
    
//     // console.log(len);
//     // console.log(doc.getText());
    
//     res.end();
//   // });
//   // console.log(content);
// });

server.listen(port);
console.log("Mindwave node app running at http://localhost:" + port);

process.title = 'sharejs';
process.on('uncaughtException', function (err) {
  console.error('An error has occurred. Please file a ticket here: https://github.com/josephg/ShareJS/issues');
  console.error('Version ' + sharejs.version + ': ' + err.stack);
});